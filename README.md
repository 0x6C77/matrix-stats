# matrix-stats

Transparent stats bot for Matrix rooms with guest access

## Getting started
Updated `settings.json` to point at a public room.

Run `node capture.js` to update stats. Stats are stored in `stats.json`

## Example stats.json
```json
{
    "processed": 1561204006525,
    "days": {
        "2019-06-20T23:00:00.000Z": 4,
        "2019-06-21T23:00:00.000Z": 1
    },
    "hours": [
        0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ],
    "totals": {
        "lines": 5,
        "words": 19
    },
    "users": {
        "test": {
            "lines": 2,
            "words": 6
        },
        "test2": {
            "lines": 3,
            "words": 13
        }
    }
}
```